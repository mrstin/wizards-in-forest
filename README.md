# Wizards In Forest

Соединение с сервером с использованием Websocket


Модели json-сообщений


Сообщения клиента:


1) CurrentLocation //текущее положение игрока

'type' = 'currentLocation'

'x' : float

'y' : float

'z' : float

'rotX' : float

'rotY' : float

'rotZ' : float


3) Damage //Сообщение о нанесении урона

'type' = 'damage'

'typeOfHit' : int


Сообщения сервера:


1) CurrentEnemyLocation //текущее положение другого игрока

'type' = 'currentEnemyLocation'

'x' : float

'y' : float

'z' : float

'rotX' : float

'rotY' : float

'rotZ' : float


3) RoundStarted

'type' = 'roundStart'

'x' : float

'y' : float

'z' : float


4) Damage //Сообщение о нанесении урона

'type' = 'damage'

'typeOfHit' : int


5) RoundOver

'type' = 'roundOver'

'result' : bool