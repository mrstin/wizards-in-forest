﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Damage{

    public string type = "damage";
    public int typeOfHit;
}
