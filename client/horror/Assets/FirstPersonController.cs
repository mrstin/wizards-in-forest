﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonController : MonoBehaviour {

    private const float FORCE = 15f;
    private const float ROTATE = 0.5f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float look = transform.eulerAngles.y * (Mathf.PI / 180);
        if (Input.GetKey(KeyCode.JoystickButton0))
        {
            transform.GetComponent<Rigidbody>().AddForce(Vector3.forward*FORCE);
        }
        if (Input.GetKey(KeyCode.JoystickButton1))
        {
            transform.GetComponent<Rigidbody>().AddForce(Vector3.left * FORCE);
        }
        if (Input.GetKey(KeyCode.JoystickButton2))
        {
            transform.GetComponent<Rigidbody>().AddForce(Vector3.right * FORCE);
        }
        if (Input.GetKey(KeyCode.JoystickButton3))
        {
            transform.GetComponent<Rigidbody>().AddForce(Vector3.back * FORCE);
        }
    }
}
